import wx


class Frame(wx.Frame):
    def __init__(self, title):
        super(Frame, self).__init__(None, title=title, size=(350, 200))
        panel = wx.Panel(self)

        box = wx.BoxSizer(wx.VERTICAL)

        text = wx.StaticText(panel, -1, "Hello World!")
        box.Add(text, 0, wx.EXPAND | wx.ALL, 10)

        edit = wx.TextCtrl(panel, style=wx.TE_MULTILINE)
        box.Add(edit, 1, wx.EXPAND | wx.ALL, 10)

        close = wx.Button(panel, wx.ID_CLOSE, "Close")
        box.Add(close, 1, wx.EXPAND | wx.ALL, 10)

        close.Bind(wx.EVT_BUTTON, self.OnClose)

        panel.SetSizer(box)
        panel.Layout()

    def OnClose(self, ev):
        self.Close()


if __name__ == "__main__":
    app = wx.App(redirect=True)
    top = Frame(title="Hello World")
    top.Show()
    app.MainLoop()
