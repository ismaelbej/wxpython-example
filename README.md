# wxPython and py2exe demo #

## How to make an wxPython executable in Windows ##

This is an example how to create an executable from a wxPython project.

## Preparation ##

Since the current version of wxPython (v3.0) only supports python 2.7,
we have to install that version. Also since recent versions of py2exe
only support python 3.0, we will need to install a version that supports
python 2.7.

We can install python 2.7 in 32 bits or 64 bits, both should work fine.
Then we will have to intall the packages of wxPython and py2exe that
match our python version.

## Creating the environment ##

TODO

## Installing required packages ##

TODO

## Creating the executable ##

We have to run inside our virtualenv

```sh
python setup.py py2exe
```

This will create a folder `dist\` which will contain
our executable `hello.exe` and the other files that are required to run it.
